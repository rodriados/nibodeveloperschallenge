import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    user: [],
    transactions: []
  },
  mutations: {
    setToken: (state, token) => state.token = token,
    setUser: (state, user) => state.user = user,
    setTransactions: (state, transactions) => state.transactions = transactions,
  },
  actions: {
    signUpUser(_, data) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .post('auth/register', data)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    loginUser({ commit }, data) {
      return new Promise((resolve, reject) => {
        Vue.axios
          .post('auth/login', data)
          .then(({ data }) => {
            commit('setToken', data.token);
            commit('setUser', data.user);
            Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${data.token}`;
            resolve(data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    fetchTransactions() {
      Vue.axios
        .get('transaction')
        .then(({ data }) => {
          this.commit('setTransactions', data);
        });
    },
    sendOFX(_, formData) {
      return new Promise((resolve, reject) => {
        const headers = {
          'Content-Type': 'multipart/form-data'
        };
        Vue.axios
          .post('transaction/upload-ofx', formData, { headers })
          .then(({data}) => resolve(data))
          .catch(error => reject(error));
      });
    }
  },
  modules: {
  }
})
