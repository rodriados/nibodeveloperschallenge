namespace Nibo.API
{
    public static class Settings
    {
        public static string JwtSecret = "e8c54355a59915d621ae86d70bd0dd3493cb61bb";
        public static int JwtHoursDuration = 2;

        public static string VueServerAddress = "http://localhost:8080";
    }
}
