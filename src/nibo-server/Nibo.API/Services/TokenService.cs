using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Nibo.API.Models;

namespace Nibo.API.Services
{
    public static class TokenService
    {
        public static string GenerateToken(User user)
        {
            byte[] jwtSecret = Encoding.ASCII.GetBytes(Settings.JwtSecret);
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", user.UserId.ToString()),
                    new Claim(ClaimTypes.Name, user.FirstName.ToString()),
                    new Claim(ClaimTypes.Email, user.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(Settings.JwtHoursDuration),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(jwtSecret),
                    SecurityAlgorithms.HmacSha512Signature)
            };

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
