using System.Security.Cryptography;
using System.Text;

namespace Nibo.API.Services
{
    public static class HashService
    {
        public static string ComputeHash<T>(T algorithm, string plainText) where T: HashAlgorithm
        {
            byte[] textBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] hashBytes = algorithm.ComputeHash(textBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hashBytes.Length; ++i)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
