using System;
using System.IO;
using System.Linq;
using MoreLinq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Nibo.API.Repositories;
using Nibo.API.Domain.Parsers;
using Nibo.API.Models;
using Nibo.API.Data;

namespace Nibo.API.Controller
{
    [ApiController]
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        
        public TransactionController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        [HttpGet]
        public ActionResult<List<dynamic>> Get()
        {
            var user = GetAuthorizedUser();
            var transactions = TransactionRepository.GetAllByUser(_appDbContext, user);

            var projection = transactions.Select(t => new
            {
                type = (t.Type == TransactionType.DEBIT ? "DEBIT" : "CREDIT"),
                amount = t.Amount,
                datePosted = t.DatePosted,
                memo = t.Memo
            });

            return Ok(projection.ToList());
        }

        [HttpGet("{id}")]
        public ActionResult<Transaction> Get(int id)
        {
            var user = GetAuthorizedUser();
            return TransactionRepository.GetById(_appDbContext, user, id);
        }

        [HttpPost]
        [Route("upload-ofx")]
        public async Task<ActionResult<dynamic>> UploadOFX(IFormFile file)
        {
            if (file.Length <= 0)
            {
                return NoContent();
            }

            var tempPath = Path.GetTempFileName();

            using (var stream = System.IO.File.Create(tempPath))
            {
                await file.CopyToAsync(stream);
            }

            var user = GetAuthorizedUser();
            var transactions = OFXParser.ParseFile(tempPath);

            // Removing the duplicated transactions from list.
            transactions = transactions.DistinctBy(t => t.Hash).ToList();

            // Let's figure out the max and min dates from the transactions
            // read from file. These will allow us to deduplicate the new transactions
            // with those already on our database.
            DateTime minDate = transactions.Min(t => t.DatePosted);
            DateTime maxDate = transactions.Max(t => t.DatePosted);

            // Get hashes from transactions already stored in the database.
            var dbHashes = TransactionRepository.GetHashesBetweenDates(_appDbContext, user, minDate, maxDate);

            // Remove transactions that are duplicates with transactions in database.
            transactions = transactions.Where(t => !dbHashes.Contains(t.Hash)).ToList();

            // Assign transactions to the currently authorized user.
            foreach (var transaction in transactions)
            {
                transaction.UserId = user.UserId;
            }

            // Finally, let's save our new transactions in the database.
            TransactionRepository.StoreMany(_appDbContext, transactions);

            return Ok(new
            {
                count = transactions.Count,
                max = maxDate,
                min = minDate
            });
        }

        private User GetAuthorizedUser()
        {
            int userId = Int32.Parse(User.FindFirst("UserId")?.Value);
            return UserRepository.GetById(_appDbContext, userId);
        }
    }
}
