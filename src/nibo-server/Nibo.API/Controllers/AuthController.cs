using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Nibo.API.Repositories;
using Nibo.API.Services;
using Nibo.API.Models;
using Nibo.API.Data;

namespace Nibo.API.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        
        public AuthController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public ActionResult<dynamic> Login([FromBody] Requests.Auth.LoginRequest request)
        {
            User user = UserRepository.GetByLogin(_appDbContext, request);

            if (user == null) {
                return NotFound(new { message = "User not found" });
            }

            return Ok(new {
                user = new {
                    id = user.UserId,
                    firstname = user.FirstName,
                    lastname = user.LastName,
                    email = user.Email
                },
                token = TokenService.GenerateToken(user)
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public ActionResult<dynamic> Register([FromBody] Requests.Auth.RegisterRequest request)
        {
            if (!ModelState.IsValid) {
                return BadRequest(new { message = "Invalid request" });
            }

            if (UserRepository.EmailAlreadyRegistered(_appDbContext, request.Email)) {
                return BadRequest(new { message = "This email has already been registered" });
            }

            UserRepository.Register(_appDbContext, request);

            return Ok(new { message = "Ok" });
        }
    }
}
