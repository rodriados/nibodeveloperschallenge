using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using Nibo.API.Services;

namespace Nibo.API.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [MaxLength(200)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(128)]
        [DataType(DataType.Password)]
        public string Password {
            get => _password;
            set => _password = HashPassword(value);
        }

        [DefaultValue(false)]
        public bool EmailVerified { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        public bool validatePassword(string plainText) =>
            (Password == HashPassword(plainText));

        public static string HashPassword(string plainText) =>
            HashService.ComputeHash(SHA512.Create(), plainText);

        private string _password;
    }
}
