using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using Nibo.API.Services;

namespace Nibo.API.Models
{
    public enum TransactionType
    {
        DEBIT,
        CREDIT
    }

    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public TransactionType Type { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DatePosted { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        [MaxLength(255)]
        public string Memo { get; set; }

        [Required]
        [MaxLength(128)]
        public string Hash { get => GetTransactionHash(); }

        public virtual User User { get; set; }

        private string GetTransactionString()
        {
            return 'T' + Type.ToString()
                 + 'D' + DatePosted.ToString()
                 + 'A' + Amount.ToString("0.00")
                 + 'M' + Memo.ToString();
        }

        private string GetTransactionHash()
        {
            string transactionString = GetTransactionString();
            return HashService.ComputeHash(SHA512.Create(), transactionString);
        }
    }
}
