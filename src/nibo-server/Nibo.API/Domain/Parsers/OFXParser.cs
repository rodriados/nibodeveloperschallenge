using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using Nibo.API.Models;

namespace Nibo.API.Domain.Parsers
{
    public static class OFXParser
    {
        // The logic applied by this "parser" is really simple: as we are limited on time
        // and there is no file validation whatsoever, we directly extract all transactions
        // found on the OFX file and return them.

        public static List<Transaction> ParseFile(string fileName)
        {
            var stream = new StreamReader(fileName);
            return ParseStream(stream);
        }

        public static List<Transaction> ParseStream(StreamReader stream)
        {
            var transactionList = new List<Transaction>();
            CultureInfo provider = new CultureInfo("en-US");

            while(!stream.EndOfStream)
            {
                // Skipping OFX file metadata.
                // If we were working on a production code, we should DEFINITIVELY check
                // and validate the file's metadata information.
                while (!stream.EndOfStream && stream.ReadLine() != "<STMTTRN>")
                {
                    continue;
                }

                // If the loop above has exhausted the stream, then we must break out of
                // the loop and return the transactions we've found.
                if (stream.EndOfStream)
                {
                    break;
                }

                string currentLine;
                var transaction = new Transaction();
                bool validTransaction = false;

                do
                {
                    currentLine = stream.ReadLine();

                    // As no file validation is performed currently, we assume the transaction is
                    // valid if at least one of the required fields is present.
                    if (currentLine.StartsWith("<TRNTYPE>"))
                    {
                        transaction.Type = currentLine.Contains("DEBIT")
                            ? TransactionType.DEBIT
                            : TransactionType.CREDIT;
                        validTransaction = true;
                    }
                    else if (currentLine.StartsWith("<DTPOSTED>"))
                    {
                        // Removing the DTPOSTED tag
                        var dateSubstring = currentLine.Substring(10, 14);
                        transaction.DatePosted = DateTime.ParseExact(dateSubstring, "yyyyMMddHHmmss", provider);
                    }
                    else if (currentLine.StartsWith("<TRNAMT>"))
                    {
                        // Removing the TRNAMT tag
                        var amountSubstring = currentLine.Substring(8);
                        transaction.Amount = Convert.ToDecimal(amountSubstring, provider);
                    }
                    else if (currentLine.StartsWith("<MEMO>"))
                    {
                        // Removing the MEMO tag
                        var memoSubstring = currentLine.Substring(6);
                        transaction.Memo = memoSubstring.Trim();
                    }
                } while (!stream.EndOfStream && currentLine != "</STMTTRN>");

                // If the read transaction is valid, then we add it to our final transaction list.
                if (validTransaction)
                {
                    transactionList.Add(transaction);
                }
            }

            return transactionList;
        }
    }
}
