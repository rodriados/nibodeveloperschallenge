using System.Linq;
using Nibo.API.Data;
using Nibo.API.Models;

namespace Nibo.API.Repositories
{
    public static class UserRepository
    {
        public static User GetById(AppDbContext appDbContext, int userId)
        {
            return appDbContext.Users
                .Find(userId);
        }

        public static User GetByLogin(AppDbContext appDbContext, Requests.Auth.LoginRequest request)
        {
            return appDbContext.Users
                .Where(user => user.Email.ToLower() == request.Email.ToLower()
                    && user.Password == User.HashPassword(request.Password))
                .FirstOrDefault();
        }

        public static async void Register(
            AppDbContext appDbContext,
            Requests.Auth.RegisterRequest request)
        {
            User user = new User {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Password = request.ConfirmPassword
            };

            await appDbContext.Users.AddAsync(user);
            await appDbContext.SaveChangesAsync();
        }

        public static bool EmailAlreadyRegistered(AppDbContext appDbContext, string email)
        {
            return null != appDbContext.Users
                .Where(user => user.Email.ToLower() == email.ToLower())
                .FirstOrDefault();
        }
    }
}
