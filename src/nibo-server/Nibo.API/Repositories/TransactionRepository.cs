using System;
using System.Collections.Generic;
using System.Linq;
using Nibo.API.Models;
using Nibo.API.Data;

namespace Nibo.API.Repositories
{
    public static class TransactionRepository
    {
        private static IQueryable<Transaction> FilterUserScope(AppDbContext appDbContext, User user)
        {
            return appDbContext.Transactions.Where(tran => tran.UserId == user.UserId);
        }

        public static List<Transaction> GetAllByUser(AppDbContext appDbContext, User user)
        {
            return FilterUserScope(appDbContext, user)
                .OrderByDescending(tran => tran.DatePosted)
                .ToList();
        }

        public static Transaction GetById(AppDbContext appDbContext, User user, int transactionId)
        {
            return FilterUserScope(appDbContext, user)
                .FirstOrDefault(tran => tran.TransactionId == transactionId);
        }

        public static List<string> GetHashesBetweenDates(
            AppDbContext appDbContext,
            User user,
            DateTime start,
            DateTime end)
        {
            return FilterUserScope(appDbContext, user)
                .Where(tran => tran.DatePosted >= start && tran.DatePosted <= end)
                .OrderByDescending(tran => tran.DatePosted)
                .Select(tran => tran.Hash)
                .ToList();
        }

        public static async void Store(AppDbContext appDbContext, Transaction transaction)
        {
            await appDbContext.Transactions.AddAsync(transaction);
            await appDbContext.SaveChangesAsync();
        }

        public static async void StoreMany(AppDbContext appDbContext, List<Transaction> transactions)
        {
            await appDbContext.Transactions.AddRangeAsync(transactions);
            await appDbContext.SaveChangesAsync();
        }
    }
}
